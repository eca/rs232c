#ifndef RS232C_DEVICE_H_
#define RS232C_DEVICE_H_
#include <cstddef>
#include <cstdint>
#include <string>
#include <cmath>
#include <stdexcept>
#include <iostream>

#include "platform/io.h"

namespace rs232c {

using std::size_t;

typedef int16_t ReadBuffer[2];
typedef int16_t WriteBuffer[2];

static_assert(sizeof(ReadBuffer) == 4, "ReadBuffer should be 4 bytes");
static_assert(sizeof(WriteBuffer) == 4, "WriteBuffer should be 4 bytes");

template<typename T>
class Point2D {
public:
    T x;
    T y;

    Point2D(T ax, T ay) : x(ax), y(ay) {}
    Point2D() : x(0), y(0) {}

    static Point2D<T> zero() {
        return Point2D<T>((T) 0, (T) 0);
    }

};

using Point = Point2D<float>;
using Vector = Point2D<float>;

class ReadData {
public:
    Point position;
    float arm_angles[2];
    Vector arm_directions[2];
    float jacobian[2][2];
};


class Device {
public:
    constexpr static const size_t READ_BUFFER_SIZE = sizeof(ReadBuffer);
    constexpr static const size_t WRITE_BUFFER_SIZE = sizeof(WriteBuffer);
    constexpr static const size_t SENSOR_STEPS = 4096;
    constexpr static const float DEGREES_PER_STEP = (float) SENSOR_STEPS / 360;
    constexpr static const float DISTANCE_FROM_CENTER_TO_MOTORS = 3.0f;
    constexpr static const float ARM_LENGTH_1 = 27.0;
    constexpr static const float ARM_LENGTH_2 = 30.0;

    explicit Device(const std::string& device_name);

    /// NOTE: Data must be at least READ/WRITE_BUFFER_SIZE
    /// TODO: Refactor this to force that at compile time
    void write(const WriteBuffer& data);
    void write(int16_t v1, int16_t v2);
    void read(ReadBuffer& data);
    void read(int16_t* v1, int16_t* v2);

    /// Read and set the initial position
    void calibrate();

    ReadData read_data();
    Point read_position() { return read_data().position; };

    ~Device();
private:
    rs232c_connection_t m_fd;
    ReadBuffer m_initial_position;
};

/// This function takes the read data, and a force vector,
/// And returns the virtual force to be applied to both motors
void virtual_force(const ReadData& data, const Vector& force, WriteBuffer& out);

template<typename T>
std::ostream& operator<<(std::ostream& os, const rs232c::Point2D<T>& p) {
    os << "{" << p.x << ", " << p.y << "}";
    return os;
}
} // namespace rs232c
#endif
