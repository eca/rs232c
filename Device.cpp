#include "Device.h"

/// TODO: Find proper naming for this class, or moving the file to a namespaced directory
namespace rs232c {

namespace {
    inline float to_radians(float degrees) {
        return degrees * M_PI / 180.0;
    }

    inline float pow2(float amount) {
        return amount * amount;
    }
}

Device::Device(const std::string& device_name) : m_initial_position{0,0} {
    m_fd = rs232c_connect(device_name.c_str());

    if ( m_fd == -1 )
        throw std::runtime_error("Couldn't connect to " + device_name + ". Reason: " + rs232c_last_os_error());
}

void Device::write(const WriteBuffer& data) {
    if ( rs232c_write(m_fd, static_cast<const void*>(data), READ_BUFFER_SIZE) == -1 )
        throw std::runtime_error(std::string("Failure writing to device. Reason: ") + rs232c_last_os_error());
}

void Device::write(int16_t v1, int16_t v2) {
    WriteBuffer data = { v1, v2 };
    write(data);
}

void Device::read(ReadBuffer& data) {
    if ( rs232c_read(m_fd, static_cast<void*>(data), READ_BUFFER_SIZE) == -1 )
        throw std::runtime_error(std::string("Failure reading from device. Reason: ") + rs232c_last_os_error());
}

/// TODO: Should we pass references instead of pointers?
void Device::read(int16_t* v1, int16_t* v2) {
    ReadBuffer data;
    read(data);
    *v1 = data[0];
    *v2 = data[1];
}

void Device::calibrate() {
    read(m_initial_position);
}

/// This function takes the read data, and a force vector,
/// And returns the virtual force to be applied to both motors
void virtual_force(const ReadData& data, const Vector& force, WriteBuffer& out) {
    float ddd = data.jacobian[0][0] * data.jacobian[1][1] - data.jacobian[0][1] * data.jacobian[1][0];

    float d11 = (data.position.x + Device::DISTANCE_FROM_CENTER_TO_MOTORS) * Device::ARM_LENGTH_1 * sin(data.arm_angles[0]) - data.position.x * Device::ARM_LENGTH_1 * cos(data.arm_angles[0]);
    float d12 = 0.0;
    float d21 = 0.0;
    float d22 = -(data.position.x + Device::DISTANCE_FROM_CENTER_TO_MOTORS) * Device::ARM_LENGTH_1 * sin(data.arm_angles[1]) - data.position.y * Device::ARM_LENGTH_1 * cos(data.arm_angles[1]);


    float tau1 = (( data.jacobian[1][1] * d11 - data.jacobian[0][1] * d21) * force.x + ( data.jacobian[1][1] * d12 - data.jacobian[0][1] * d22) * force.y) / ddd;
    float tau2 = ((-data.jacobian[1][0] * d11 + data.jacobian[0][0] + d21) * force.x + (-data.jacobian[1][0] * d12 + data.jacobian[0][0] * d22) * force.y) / ddd;

    tau1 = std::max(tau1, -20000.0f);
    tau1 = std::min(tau1, 20000.0f);

    tau2 = std::max(tau2, -20000.0f);
    tau2 = std::min(tau2, 20000.0f);

    out[0] = -tau1;
    out[1] = tau2;
}

ReadData Device::read_data() {
    ReadBuffer read_buffer;
    ReadData data;

    read(read_buffer);

    // We calculate how many steps have we moved in each direction
    // and use it to calculate the angles that the first arms form.
    //
    // The left angle is calculated counter-clockwise, and the right one clockwise
    //
    // Since the left arm was calibrated at ninety degrees position we must
    // add ninety to get the right angle.

    float deg_angle_1 = 90.0f + (read_buffer[0] - m_initial_position[0]) / DEGREES_PER_STEP;
    float deg_angle_2 = -(read_buffer[1] - m_initial_position[1]) / DEGREES_PER_STEP;
    float angle_1 = to_radians(deg_angle_1);
    float angle_2 = to_radians(deg_angle_2);

    data.arm_angles[0] = angle_1;
    data.arm_angles[1] = angle_2;

    // Those are the coordinates of the two axes (the ends of the first arms)
    // ARM_LENGHT_1 is the hypotenuse of the triangle formed by the coords
    //
    // Take into account that the coordinates are from the motors position,
    // and y is going down
    float x1 = - DISTANCE_FROM_CENTER_TO_MOTORS - ARM_LENGTH_1 * cos(angle_1);
    float y1 = - ARM_LENGTH_1 * sin(angle_1);

    float x2 = DISTANCE_FROM_CENTER_TO_MOTORS + ARM_LENGTH_1 * cos(angle_2);
    float y2 = - ARM_LENGTH_1 * sin(angle_2);

    // This is the distance between the point (x1, y1) and the point (x2, y2)
    float r = sqrt( pow2(x2-x1) + pow2(y2-y1) );

    // With that distance we can calculate alpha, which is the angle between
    // r and the second arm
    float cos_alpha = r / (2 * ARM_LENGTH_2);
    float alpha = acos(cos_alpha);

    // This angle is the angle between r and the normal
    // doing so, with alpha and theta, we can obtain
    // a rectangle triangle to calculate px and py
    float theta = atan( (y2-y1) / (x2-x1) );

    // Now we can know the absolute coordinates using that triangle
    float px = x1 + ARM_LENGTH_2 * cos(alpha - theta);
    float py = y1 - ARM_LENGTH_2 * sin(alpha - theta);

    data.position = Point(px, py);

#ifdef DEBUG
    std::cout << "r: " << r << ", Alpha: " << alpha << ", Theta: " << theta << std::endl;
    std::cout << "Orig { " << m_initial_position[0] << ", " << m_initial_position[1] << " }" << std::endl;
    std::cout << "Read { " << read_buffer[0] << ", " << read_buffer[1] << " }" << std::endl;
    std::cout << "Deg { " << deg_angle_1 << "," << deg_angle_2 << " }" << std::endl;
    std::cout << "Rad { " << angle_1 << "," << angle_2 << " }" << std::endl;
    std::cout << "Pos " << data.position <<  std::endl;
#endif

    // Vector
    // dx1
    data.jacobian[0][0] = px + DISTANCE_FROM_CENTER_TO_MOTORS + ARM_LENGTH_1 * cos(angle_1);
    // dx2
    data.jacobian[0][1] = px - DISTANCE_FROM_CENTER_TO_MOTORS - ARM_LENGTH_1 * cos(angle_2);
    // dy1
    data.jacobian[1][0] = py + ARM_LENGTH_1 * sin(angle_1);
    // dy2
    data.jacobian[1][1] = py + ARM_LENGTH_1 * sin(angle_2);

    return data;
}

Device::~Device() {
    rs232c_disconnect(m_fd);
}
} // namespace rs232c

