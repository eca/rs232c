#ifndef RS232C_IO_H_
#define RS232C_IO_H_

/// The platform specific file is required to:
/// * `typedef` the connection type
/// * define RS232C_INVALID_CONNECTION (an invalid connection type)
/// * define or provide a function to return a string representing the last OS error
///     called rs232c_last_os_error()
#ifdef PLATFORM_WINDOWS
#include "windows/io.h"
#else
#include "unix/io.h"
#endif

/// Connect to the rs232 using the specified device
/// Returns the port fd on success
rs232c_connection_t rs232c_connect(const char* const device);

/// Disconnect by flushing the data and closing the file
/// descriptor
void rs232c_disconnect(rs232c_connection_t conn);

/// Read `len` bytes from the fd and store them in `buff`
/// Returns 0 on success and -1 on failure
int rs232c_read(rs232c_connection_t conn, void* buff, size_t len);

/// Write `len` bytes to `fd`
/// Returns 0 on success and -1 on failure
int rs232c_write(rs232c_connection_t conn, const void* buff, size_t len);
#endif
