#include "io.h"

/// Connect to the rs232 using the specified device
/// Returns the port fd on success
rs232c_connection_t rs232c_connect(const char* const device) {
    BOOL fRetVal;
    DCB dcb;
    COMMTIMEOUTS  timeout;

    /// Open the device
    HANDLE port = CreateFile(device, 
                             GENERIC_READ | GENERIC_WRITE,
                             0,
                             NULL,
                             OPEN_EXISTING,
                             0,
                             NULL);

    if ( port == INVALID_HANDLE_VALUE )
        return INVALID_HANDLE_VALUE;

    /// Flush the buffers
    if ( ! PurgeComm(port, PURGE_TXCLEAR | PURGE_RXCLEAR) )
       goto error; 

    /// Get the info
    if ( ! GetCommState(port, &dcb) ) 
        goto error;

    dcb.DCBlength = sizeof(DCB);
    dcb.BaudRate = 115200;
    dcb.ByteSize = 8;
    dcb.Parity   = NOPARITY;
    dcb.StopBits = ONESTOPBIT;

    if ( ! SetCommState(port, &dcb) )
        goto error;

    timeout.ReadIntervalTimeout = 0xFFFFFFFF;
    timeout.ReadTotalTimeoutMultiplier = 0;
    timeout.ReadTotalTimeoutConstant = 1000;
    timeout.WriteTotalTimeoutMultiplier = 0;
    timeout.WriteTotalTimeoutConstant = 1000;

    if ( ! SetCommTimeouts(port, &timeout) )
        goto error;

    if ( ! SetupComm(port, 128, 128) )
        goto error;

    return port;

error:
    CloseHandle(port);
    return INVALID_HANDLE_VALUE;
}

/// Disconnect by flushing the data and closing the file
/// descriptor
void rs232c_disconnect(rs232c_connection_t conn) {
    FlushFileBuffers(conn);
    CloseHandle(conn);
}

/// Read `len` bytes from the fd and store them in `buff`
/// Returns 0 on success and -1 on failure
int rs232c_read(rs232c_connection_t conn, void* buff, size_t len) {
    DWORD read = 0;
    DWORD read_this_time;

    while ( read != len ) {
        if ( ! ReadFile(conn, ((char*)buff) + read, len - read, &read_this_time, NULL) )
            return -1;
        read += read_this_time;
    }

    return 0;
}

/// Write `len` bytes to `fd`
/// Returns 0 on success and -1 on failure
int rs232c_write(rs232c_connection_t conn, const void* buff, size_t len) {
    DWORD written = 0;
    DWORD written_this_time;

    while ( written != len ) {
        if ( ! WriteFile(conn, ((char*)buff) + written, len - written, &written_this_time, NULL) )
            return -1;
        written += written_this_time;
    }

    return 0;
}
