#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

/// In windows the connection type is a HANDLE
typedef HANDLE rs232c_connection_t;

#define RS232C_INVALID_CONNECTION INVALID_HANDLE_VALUE

/// TODO: Consider allocating the buffer on the runtime, since right now this is not
///       thread-safe
char* rs232c_last_os_error() {
    static char error_buffer[250] = { 0 };
    DWORD error_code = GetLastError();

    snprintf(buffer, sizeof(buffer), "Windows error code %ld (%lx)", error_code, error_code);

    return error_buffer;
}
