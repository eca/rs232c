#include <stdio.h>

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>

/// In *nix this is just a file descriptor
typedef int rs232c_connection_t;

#define RS232C_INVALID_CONNECTION (-1)

#define rs232c_last_os_error() strerror(errno)
