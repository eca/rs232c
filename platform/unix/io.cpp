#include "io.h"

/// Connect to the rs232 using the specified device
/// Returns the port fd on success
rs232c_connection_t rs232c_connect(const char* const device) {
    struct termios device_data;
    // TODO: (use O_NONBLOCK if necessary)
    int port = open(device, O_RDWR);

    if ( port == -1 )
        return -1;

    // Get the current device data
    tcgetattr(port, &device_data);

    cfmakeraw(&device_data);

    // Set the baud rate
    cfsetispeed(&device_data, B115200);
    cfsetospeed(&device_data, B115200);

    // Set buffer size to 1 byte
    device_data.c_cc[VTIME] = 0;
    device_data.c_cc[VMIN] = 1;

    // Make the changes and flush the buffers
    if ( tcsetattr(port, TCSAFLUSH, &device_data) == -1 ) {
        close(port);
        return -1;
    }

    return port;
}

/// We ignore errors on purpose here
void rs232c_disconnect(int fd) {
    if ( fd == -1 )
        return;

    tcflush(fd, TCIOFLUSH);
    close(fd);
}

/// Read `len` bytes from the fd and store them in `buff`
/// Returns 0 on success and -1 on failure
int rs232c_read(int fd, void* buff, size_t len) {
    size_t bytes_read = 0;
    ssize_t bytes_read_this_time;

    while ( bytes_read != len ) {
        bytes_read_this_time = read(fd, ((char*) buff) + bytes_read, len - bytes_read);
        if ( bytes_read_this_time == -1 ) {
            if ( errno == EAGAIN )
                continue;
            else
                return -1;
        }

        bytes_read += (size_t) bytes_read_this_time;
    }

    return 0;
}

/// Write `len` bytes to `fd`
/// Returns 0 on success and -1 on failure
int rs232c_write(int fd, const void* buff, size_t len) {
    size_t bytes_written = 0;
    ssize_t bytes_written_this_time;

    while ( bytes_written != len ) {
        bytes_written_this_time = write(fd, ((char*) buff) + bytes_written, len - bytes_written);
        if ( bytes_written_this_time == -1 )
            return -1;

        bytes_written += (size_t) bytes_written_this_time;
    }

    return 0;
}

